## Public Subnet to provision the EC2 instance within

data aws_subnet "target_public_subnet" {
  id = var.target_subnet_id
}

data aws_vpc "bootcamp_vpc" {
  id = var.bootcamp_sg_vpc_id
}

## Create keypair to use for access
resource "aws_key_pair" "bootcamp_keypair" {
  public_key = var.public_key
}

## EC2 security group
resource "aws_security_group" "bootcamp_instance_sg" {
  # Just hard coding for now for simplicity
  name = "devops-bootcamp-sg"

  vpc_id = data.aws_vpc.bootcamp_vpc.id

  # Just add SSH ingress for the provided set
  ingress {
    from_port = 22
    protocol  = "TCP"
    to_port   = 22
    cidr_blocks = [
      # cycle through all provided IPs in var.whitelisted_ips to allow SSH access
      for cidr in var.whitelisted_ips:
            cidr
    ]
  }

  egress {
    from_port = 0
    protocol  = "-1"
    to_port   = 0
  }
}

## Provisioning the Test EC2 instance
# Taken directly from the hashicorp docs: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance
data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "test_ec2" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t3.medium" # t3.medium is good enough for testing
  key_name = aws_key_pair.bootcamp_keypair.key_name
  vpc_security_group_ids = [  # Add the 1 security group from above to the EC2 for access
    aws_security_group.bootcamp_instance_sg.id
  ]

  subnet_id = data.aws_subnet.target_public_subnet.id

  # See tfvars/dev.use1.tfvars for the variable here
  # you could fill out a heredoc as well here. Replace var.userdata with <<EOF .... EOF
  user_data = var.userdata
  tags = {
    Name = "devops-bootcamp instance"
    Environment = "dev"
  }
}

output "ec2_public_ip" {
  value = "SSH here to access your instance: ssh ubuntu@${aws_instance.test_ec2.public_ip}"
}