# VPC Id to create the security group in. Must have the subnet from below
bootcamp_sg_vpc_id = "vpc-xxxxxx"

# Public Subnet ID goes here. Public is not production ready, but is quick and easy to work with
target_subnet_id = "subnet-xxxxxxx"

# Add comma-separated set of IP cidrs here to allow SSH access to the instance
whitelisted_ips = [
        ""
]

# Paste in your public ssh key for SSH access
public_key = ""

# (Optional) Add userdata for the EC2 to run when it starts up
userdata = <<EOF
#!/bin/bash
# Script goes here
EOF
