variable "bootcamp_sg_vpc_id" {
  type = string
  description = "VPC ID to create the security group within"
}

variable "target_subnet_id" {
  type = string
  description = "Target Subnet ID to deploy the bootcamp instance within. Should be Public for ease of access"
}

variable "whitelisted_ips" {
  type = set(string)
  description = "Set of IP address CIDRs to allow SSH access to the bootcamp instance"
}

variable "public_key" {
  type = string
  description = "Public SSH Key half to allow SSH access to the bootcamp instance"
}

variable "userdata" {
  type = string
  description = "Optional Userdata ( startup script ) to have the bootcamp instance run when starting"
  default = ""
}

